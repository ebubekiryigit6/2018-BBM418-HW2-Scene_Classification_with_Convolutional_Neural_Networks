REM Learning Rate (RUNNING ON SERVER)
C:\ProgramData\Anaconda2\envs\vision\python.exe ./main.py -m vgg16 -e 8 -b 32 -w 0 -f 26 -l 0.001

C:\ProgramData\Anaconda2\envs\vision\python.exe ./main.py -m vgg16 -e 8 -b 32 -w 0 -f 26 -l 0.0001

C:\ProgramData\Anaconda2\envs\vision\python.exe ./main.py -m vgg16 -e 8 -b 32 -w 0 -f 26 -l 0.00001




REM resnet freeze all layers
C:\ProgramData\Anaconda2\envs\vision\python.exe ./main.py -m resnet18 -e 10 -b 64 -w 0 -f 51 -l 0.001

REM resnet freeze little bit layers
C:\ProgramData\Anaconda2\envs\vision\python.exe ./main.py -m resnet18 -e 10 -b 64 -w 0 -f 27 -l 0.001


REM increase epoch for learning rate
C:\ProgramData\Anaconda2\envs\vision\python.exe ./main.py -m vgg16 -e 15 -b 64 -w 0 -f 26 -l 0.00001

REM freeze all of layers
C:\ProgramData\Anaconda2\envs\vision\python.exe ./main.py -m vgg16 -e 8 -b 64 -w 0 -f 32 -l 0.001

REM freeze little bit layers
C:\ProgramData\Anaconda2\envs\vision\python.exe ./main.py -m vgg16 -e 8 -b 64 -w 0 -f 16 -l 0.001

