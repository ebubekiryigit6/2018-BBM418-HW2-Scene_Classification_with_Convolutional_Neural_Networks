import torch.utils.data as data
from PIL import Image
import os
import os.path

"""

We use torchvision/datasets/folder.py ImageFolder class
But it uses parent of "train" and "test" image folders.
Our dataset is mixed. Train and test images in same folder.
By making changes in this class I made the ImageFolder initializer able to read our dataset. 
After making the required edits, a pull request will be sent to GitHub.

"""


def getClassList(flist):
    """
    finds all classes in given dataset and indexes 0 to len class list

    :param flist: text file that contains path of Image data
    :return: returns a tuple of class list and class index (class list , class index dict)
    """
    classes = []
    with open(flist, 'r') as rf:
        for line in rf.readlines():
            line = line.strip().strip("\n").strip()
            label = line.split("/")[0]  # label or class of image
            if label not in classes:
                classes.append(label)
    classes.sort()
    # index classes
    class_to_idx = {classes[i]: i for i in range(len(classes))}
    return classes, class_to_idx


def pil_loader(path):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert('RGB')


def default_loader(path):
    # open as rgb image
    return pil_loader(path)


def default_flist_reader(flist, class_to_idx):
    """
    flist format: impath1
                  impath2
                  .
                  .
                  .
    """
    imlist = []
    with open(flist, 'r') as rf:
        for line in rf.readlines():
            line = line.strip().strip("\n").strip()
            if line:
                impath = line.strip()
                imlabel = impath.split("/")[0]
                imlist.append((impath, class_to_idx[imlabel]))

    return imlist


class ImageFilelist(data.Dataset):

    def __init__(self, root, flist, transform=None, target_transform=None,
                 flist_reader=default_flist_reader, loader=default_loader):

        classes, class_to_idx = getClassList(flist)

        self.class_to_idx = class_to_idx
        self.classes = classes
        self.root = root
        self.imlist = flist_reader(flist, self.class_to_idx)
        self.transform = transform
        self.target_transform = target_transform
        self.loader = loader

    def __getitem__(self, index):
        impath, target = self.imlist[index]
        img = self.loader(os.path.join(self.root, impath))
        if self.transform is not None:
            img = self.transform(img)
        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        return len(self.imlist)
