import optparse
import time
import pickle
import os

import constants


def getCpuCount():
    return os.cpu_count()


def parseArguments(argv):
    """
    Parse argument options and returns option list

    :param argv: program arguments
    :return: option list
    """
    parser = optparse.OptionParser()
    parser.add_option('-m', '--model',
                      action="store", dest="model_to_use",
                      help="Pretrained Model for finetuning", default=constants.DEFAULT_PRETRAINED_MODEL)
    parser.add_option('-e', '--epoch',
                      action="store", dest="num_epoch",
                      help="Number of Epoch (Integer)", default=constants.DEFAULT_EPOCH_NUM)
    parser.add_option('-b', '--batch',
                      action="store", dest="batch_size",
                      help="Batch Size (Integer)", default=constants.DEFAULT_BATCH_SIZE)
    parser.add_option('-w', '--worker',
                      action="store", dest="worker_num",
                      help="Number of Workers (Integer)", default=constants.DEFAULT_WORKER_NUM)
    parser.add_option('-f', '--freeze',
                      action="store", dest="freeze_data_num",
                      help="Number of Layers to freeze (Integer)", default=constants.DEFAULT_FREEZE_LAYER_NUM)
    parser.add_option('-l', '--learning-rate',
                      action="store", dest="learning_rate",
                      help="Learning Rate (Float)", default=constants.DEFAULT_LEARNING_RATE)
    options, args = parser.parse_args(argv)
    return options


def writeLog(tag, log):
    """
    Writes logs to console log file.

    :param tag: log tag
    :param log: log message
    """
    f = open(constants.LOG_FILE, "a")
    start = time.strftime("%H:%M:%S") + "   LOG/ " + tag.upper() + ":  "
    log = log.replace("\n", "\n" + (" " * len(start)))
    buffer = start + log + "\n"
    f.write(buffer)
    f.close()


def writeAsSerializable(array, fileName):
    """
    Writes array to file as serializable.

    :param array: array to dump
    :param fileName: which file?
    """
    file = open(fileName, 'wb')
    pickle.dump(array, file)
    file.close()
    writeLog("PICKLE", "PICKLE file is created as " + fileName)


def getObjectFromPickle(fileName):
    """
    Gets array from pickle file.

    :param fileName: file that contains binary values of array
    :return: an array from given file
    """
    file = open(fileName, 'rb')
    array = pickle.load(file)
    file.close()
    writeLog("PICKLE", "Array is get from pickle file " + fileName)
    return array
