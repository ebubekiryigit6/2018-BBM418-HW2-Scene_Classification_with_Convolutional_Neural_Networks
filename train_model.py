from torch.optim import lr_scheduler
import torch
from torch.autograd import Variable
from torchvision import transforms, models
import time

import constants
import util
import image_loader as imloader


def train_model(data_loaders, data_sizes, model, criterion, optimizer, scheduler, num_epochs):
    util.writeLog("TRAINING", "Train is started with"
                  + "\nTrain Size: " + str(data_sizes[constants.TRAIN_DATA_LABEL])
                  + "\nTest Size:  " + str(data_sizes[constants.TEST_DATA_LABEL]))

    since = time.time()

    best_acc = 0.0

    # data for each epochs"
    trainLoss = []
    trainCorrect = []
    trainTop5 = []
    testLoss = []
    testCorrect = []
    testTop5 = []

    for epoch in range(num_epochs):
        util.writeLog("EPOCH-LOGGER", 'Epoch {}/{}'.format(epoch, num_epochs - 1))

        # Each epoch has a training and validation phase
        for phase in [constants.TRAIN_DATA_LABEL, constants.TEST_DATA_LABEL]:
            if phase == constants.TRAIN_DATA_LABEL:
                scheduler.step()
                model.train(True)  # Set model to training mode
            else:
                model.train(False)  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0
            top5_precs = 0

            # Iterate over data.
            for data in data_loaders[phase]:
                # get the inputs
                inputs, labels = data

                # wrap them in Variable
                if constants.USE_GPU:
                    inputs = Variable(inputs.cuda())
                    labels = Variable(labels.cuda())
                else:
                    inputs, labels = Variable(inputs), Variable(labels)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                outputs = model(inputs)
                _, preds = torch.max(outputs.data, 1)
                _5, preds5 = torch.topk(outputs, 5)
                loss = criterion(outputs, labels)

                for i in range(len(preds5)):
                    top5_precs += int(torch.sum(labels[i] == preds5[i]))

                # backward + optimize only if in training phase
                if phase == constants.TRAIN_DATA_LABEL:
                    loss.backward()
                    optimizer.step()

                # statistics
                running_loss += loss.data[0] * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / data_sizes[phase]
            epoch_acc = running_corrects / data_sizes[phase]
            epoch_acc_top5 = top5_precs / data_sizes[phase]

            util.writeLog("EPOCH-LOGGER",
                          '{} Loss: {:.4f} Top1_Acc: {:.4f}  Top5_Acc: {:.4f}'.format(phase, epoch_loss, epoch_acc,
                                                                                      epoch_acc_top5))

            if phase == constants.TEST_DATA_LABEL and epoch_acc > best_acc:
                best_acc = epoch_acc

            if phase == constants.TRAIN_DATA_LABEL:
                trainCorrect.append(epoch_acc)
                trainLoss.append(epoch_loss)
                trainTop5.append(epoch_acc_top5)
            else:
                testCorrect.append(epoch_acc)
                testLoss.append(epoch_loss)
                testTop5.append(epoch_acc_top5)

    time_elapsed = time.time() - since

    util.writeLog("DATA-LOGGER", "Train Top1 Acc: " + str(trainCorrect))
    util.writeLog("DATA-LOGGER", "Train Loss: " + str(trainLoss))
    util.writeLog("DATA-LOGGER", "Train Top5 Acc: " + str(trainTop5))

    util.writeLog("DATA-LOGGER", "Test Top1 Acc: " + str(testCorrect))
    util.writeLog("DATA-LOGGER", "Test Loss: " + str(testLoss))
    util.writeLog("DATA-LOGGER", "Test Top5 Acc: " + str(testTop5))

    util.writeLog("TRAINING", 'Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    util.writeLog("TRAINING", 'Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    # model.load_state_dict(best_model_wts)
    # return model


def getTrainValConfigs():
    train_config = imloader.ImageFilelist(root=constants.IMAGES_FOLDER_PATH,
                                          flist=constants.TRAIN_IMAGES_TEXT_FILE,
                                          transform=transforms.Compose([
                                              transforms.Resize(256),
                                              transforms.CenterCrop(224),
                                              transforms.ToTensor(),
                                              transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                          ]))

    val_config = imloader.ImageFilelist(root=constants.IMAGES_FOLDER_PATH,
                                        flist=constants.TEST_IMAGES_TEXT_FILE,
                                        transform=transforms.Compose([
                                            transforms.Resize(256),
                                            transforms.CenterCrop(224),
                                            transforms.ToTensor(),
                                            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                        ]))

    return train_config, val_config


def vgg16(class_names, freeze_data_num, learning_rate):
    model_ft = models.vgg16(pretrained=True)

    if constants.USE_GPU:
        model_ft = model_ft.cuda()

    criterion = torch.nn.CrossEntropyLoss()
    # Observe that all parameters are being optimized

    counter = 0
    for param in model_ft.parameters():
        param.requires_grad = False
        counter += 1
        if counter == freeze_data_num:
            break

    model_ft.classifier._modules['6'] = torch.nn.Linear(4096, len(class_names))

    optimizer_ft = torch.optim.SGD(filter(lambda p: p.requires_grad, model_ft.parameters()),
                                   lr=learning_rate,
                                   momentum=0.9)

    # Decay LR by a factor of 0.1 every 7 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft,
                                           step_size=7,
                                           gamma=0.1)

    return model_ft, criterion, optimizer_ft, exp_lr_scheduler


def resnet18(class_names, freeze_data_num, learning_rate):
    model_ft = models.resnet18(pretrained=True)

    # 3 set of 18 (We can freeze 0 to 54 layers)
    # we choose 48 (freeze 16 sets)
    counter = 0
    for param in model_ft.parameters():
        param.requires_grad = False
        counter += 1
        if counter == freeze_data_num:
            break

    num_ftrs = model_ft.fc.in_features
    model_ft.fc = torch.nn.Linear(num_ftrs, len(class_names))

    if constants.USE_GPU:
        model_ft = model_ft.cuda()

    criterion = torch.nn.CrossEntropyLoss()

    # Observe that all parameters are being optimized
    optimizer_ft = torch.optim.SGD(filter(lambda p: p.requires_grad, model_ft.parameters()),
                                   lr=learning_rate,
                                   momentum=0.9)

    # Decay LR by a factor of 0.1 every 7 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

    return model_ft, criterion, optimizer_ft, exp_lr_scheduler


def startTrainingWithDataLoaders(options):
    # get the option args
    num_of_epochs = int(options.num_epoch)
    batch_size = int(options.batch_size)
    worker_num = int(options.worker_num)
    learning_rate = float(options.learning_rate)
    freeze_data_num = int(options.freeze_data_num)
    model_to_use = options.model_to_use

    util.writeLog("ARG-PARSER", "Argument Informations:"
                  + "\nMODEL: " + options.model_to_use
                  + "\nEPOCH: " + options.num_epoch
                  + "\nBATCH SIZE: " + options.batch_size
                  + "\nWORKERS: " + options.worker_num
                  + "\nLEARNING RATE: " + str(options.learning_rate)
                  + "\nFREEZE LAYER NUM: " + str(options.freeze_data_num))

    util.writeLog("GPU-ENGINE", "GPU Enabled: " + str(constants.USE_GPU))
    util.writeLog("MODEL-FREEZER", "Freeze Model Parameters: " + str(freeze_data_num))

    train_config, val_config = getTrainValConfigs()
    train_loader = torch.utils.data.DataLoader(train_config,
                                               batch_size=batch_size, shuffle=True,
                                               num_workers=worker_num)
    val_loader = torch.utils.data.DataLoader(val_config,
                                             batch_size=batch_size, shuffle=True,
                                             num_workers=worker_num)
    util.writeLog("DATA-LOADER", "Train and Test Data has been loaded.")
    data_loaders = {constants.TRAIN_DATA_LABEL: train_loader,
                    constants.TEST_DATA_LABEL: val_loader}
    data_sizes = {constants.TRAIN_DATA_LABEL: len(train_config),
                  constants.TEST_DATA_LABEL: len(val_config)}

    # get class lists
    class_names = train_config.classes

    if model_to_use.lower() == constants.RESNET18.lower():
        model_ft, criterion, optimizer_ft, exp_lr_scheduler = resnet18(class_names, freeze_data_num, learning_rate)
    else:
        # for this case, only supported resnet18 and vgg16
        model_ft, criterion, optimizer_ft, exp_lr_scheduler = vgg16(class_names, freeze_data_num, learning_rate)

    train_model(data_loaders, data_sizes, model_ft, criterion, optimizer_ft, exp_lr_scheduler,
                num_of_epochs)
