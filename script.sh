#!/usr/bin/env bash

# epochs
python3 main.py -m vgg16 -e 5 -b 32 -w 4 -f 26 -l 0.001
python3 main.py -m vgg16 -e 10 -b 32 -w 4 -f 26 -l 0.001
python3 main.py -m vgg16 -e 15 -b 32 -w 4 -f 26 -l 0.001
python3 main.py -m vgg16 -e 20 -b 32 -w 4 -f 26 -l 0.001
python3 main.py -m vgg16 -e 30 -b 32 -w 4 -f 26 -l 0.001

# batch size
python3 main.py -m vgg16 -e 8 -b 16 -w 4 -f 26 -l 0.001
python3 main.py -m vgg16 -e 8 -b 32 -w 4 -f 26 -l 0.001
python3 main.py -m vgg16 -e 8 -b 64 -w 4 -f 26 -l 0.001
python3 main.py -m vgg16 -e 8 -b 128 -w 4 -f 26 -l 0.001

# learning rate
python3 main.py -m vgg16 -e 8 -b 32 -w 4 -f 26 -l 0.001
python3 main.py -m vgg16 -e 8 -b 32 -w 4 -f 26 -l 0.0001
python3 main.py -m vgg16 -e 8 -b 32 -w 4 -f 26 -l 0.00001



# resnet18
python3 main.py -m resnet18 -e 8 -b 32 -w 4 -f 48 -l 0.001
python3 main.py -m resnet18 -e 16 -b 32 -w 4 -f 48 -l 0.001
python3 main.py -m resnet18 -e 25 -b 32 -w 4 -f 48 -l 0.001

python3 main.py -m resnet18 -e 8 -b 16 -w 4 -f 48 -l 0.001
python3 main.py -m resnet18 -e 8 -b 32 -w 4 -f 48 -l 0.001
python3 main.py -m resnet18 -e 8 -b 64 -w 4 -f 48 -l 0.001
python3 main.py -m resnet18 -e 8 -b 128 -w 4 -f 48 -l 0.001

python3 main.py -m resnet18 -e 5 -b 32 -w 4 -f 48 -l 0.001
python3 main.py -m resnet18 -e 5 -b 32 -w 4 -f 48 -l 0.0001
python3 main.py -m resnet18 -e 5 -b 32 -w 4 -f 48 -l 0.00001

