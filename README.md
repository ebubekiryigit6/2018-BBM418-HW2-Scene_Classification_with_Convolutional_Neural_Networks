# Scene Classification with Convolutional Neural Networks

BBM418 - Computer Vision Lab.

Assignment 2

### Author
Ebubekir Yiğit - 21328629



### Project Introduction
In this project, we will try to fine-tuning using Convolutional Neural Networks (CNN). 
In doing so, instead of training the network from scratch, 
we are making changes to these models using previously trained models. 
We then fine-tuned this model by training it. 
In this project, we will use VGG16 and Resnet18 and 
find the Linear layers of these models and put them in their own way. 
We will then begin to train the model by determining which layers to free.


## Getting Started

You can browse the "script" files to run the entire process of the project.

I can not submit because the Submit system does not support cmd and sh files :)

For Windows:
```bash
    > script.cmd
```

For Linux:
```bash
    > sh script.sh
```


**All operations are progressively saved to the console.log file. 
You can browse the console.log file to see the operations.**




### Prerequisites

Python 3.5 is required to run the project. 
I'm sorry for the version of Python but i cant install PyTorch with Python 2.7 in Anaconda.

Python External Libraries:

    PyTorch 0.3.1 - TorchVision - Numpy - Pillow 
    
If you are having trouble installing PyTorch on Microsoft Windows:

-   Note that your "pip" version is 10.0 and above.
-   Install Anaconda 2 or Anaconda 3
-   Open your Anaconda Prompt as Administrator
-   Run 'conda create -n vision_env python=3.5 numpy pyyaml mkl' to create your environment
-   Run 'conda activate vision_env' to activate environment
-   Run 'pip install --upgrade pip' to update pip
-   Run 'conda install -c peterjc123 pytorch' to install PyTorch (This command installs latest version of Pytorch if you want to install different version please use pytorch='0.3.1')
-   Run 'pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew'
-   Run 'pip install kivy.deps.gstreamer'
-   If it doesnt help, please see 'https://www.superdatascience.com/pytorch/'


### Running the Project

    python3.5 main.py [options]
    
    Options:
        -m <model_to_use> (supports vgg16 and resnet18)
        -e <num_of_epoch> (How many epoch?)
        -b <batch_size> (Batch Size of network)
        -w <num_of_workers> (Process number - it depends data size, batch size and epoch size)
        -f <num_of_layers_to_freeze> (How many layers will freeze?)
        -l <learning_rate> (I think optimum is 0.001)
        
    Default Options: (If you dont write any options)
        -m vgg16
        -e 4
        -b 64
        -w 4
        -f 26
        -l 0.001
        and also default GPU Usage: False


#### USAGE

To Run with default configs:
```
python3.5 main.py
```
-   Program uses: VGG16 - 4 Epoch - 64 Batch - 4 Process - Freeze 26 Layers - 0.001 Learning Rate

-   Output: Loss, Train Accuracy, Test Accuracy, Top 5 Accuracy are shown in console.log file



To Run with your own configs:

    python3.5 main.py -m resnet18 -e 2 -b 32 -w 0 -f 48 -l 0.001
    
-   0 (Zero) worker is only uses main process

-   Output: Loss, Train Accuracy, Test Accuracy, Top 5 Accuracy are shown in console.log file


**After run, please see "console.log" file to show process logs.**

### console.log File
```text
19:29:24   LOG/ MAIN-PROCESS:  Scene Classification with Convolutional Neural Networks program is started.
19:29:24   LOG/ ARG-PARSER:  Argument Informations:
                             MODEL: vgg16
                             EPOCH: 5
                             BATCH SIZE: 32
                             WORKERS: 4
                             LEARNING RATE: 0.001
                             FREEZE LAYER NUM: 26
19:29:24   LOG/ GPU-ENGINE:  GPU Enabled: False
19:29:24   LOG/ MODEL-FREEZER:  Freeze Model Parameters: 26
19:29:24   LOG/ DATA-LOADER:  Train and Test Data has been loaded.
19:29:33   LOG/ TRAINING:  Train is started with
                           Train Size: 1606
                           Test Size:  394
19:29:33   LOG/ EPOCH-LOGGER:  Epoch 0/4
19:46:42   LOG/ EPOCH-LOGGER:  train Loss: 1.7649 Top1_Acc: 0.4975  Top5_Acc: 0.7914
19:50:53   LOG/ EPOCH-LOGGER:  val Loss: 0.7609 Top1_Acc: 0.7919  Top5_Acc: 0.9797
19:50:53   LOG/ EPOCH-LOGGER:  Epoch 1/4
20:07:55   LOG/ EPOCH-LOGGER:  train Loss: 0.6007 Top1_Acc: 0.8076  Top5_Acc: 0.9913
20:12:06   LOG/ EPOCH-LOGGER:  val Loss: 0.6014 Top1_Acc: 0.8274  Top5_Acc: 0.9898
20:12:06   LOG/ EPOCH-LOGGER:  Epoch 2/4
20:29:14   LOG/ EPOCH-LOGGER:  train Loss: 0.4012 Top1_Acc: 0.8767  Top5_Acc: 0.9969
20:33:23   LOG/ EPOCH-LOGGER:  val Loss: 0.5599 Top1_Acc: 0.8376  Top5_Acc: 0.9822
20:33:23   LOG/ EPOCH-LOGGER:  Epoch 3/4
20:50:23   LOG/ EPOCH-LOGGER:  train Loss: 0.2852 Top1_Acc: 0.9184  Top5_Acc: 0.9981
20:54:32   LOG/ EPOCH-LOGGER:  val Loss: 0.5753 Top1_Acc: 0.8223  Top5_Acc: 0.9848
20:54:32   LOG/ EPOCH-LOGGER:  Epoch 4/4
21:11:36   LOG/ EPOCH-LOGGER:  train Loss: 0.2234 Top1_Acc: 0.9321  Top5_Acc: 0.9981
21:15:46   LOG/ EPOCH-LOGGER:  val Loss: 0.5743 Top1_Acc: 0.8325  Top5_Acc: 0.9873
21:15:46   LOG/ TRAINING:  Training complete in 106m 13s
21:15:46   LOG/ TRAINING:  Best val Acc: 0.837563
21:15:46   LOG/ MAIN-PROCESS:  Scene Classification with Convolutional Neural Networks program is finished in 6381.552483558655 seconds.
```
