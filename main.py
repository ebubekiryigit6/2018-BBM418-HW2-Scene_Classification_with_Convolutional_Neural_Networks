import sys
import time

import util
import train_model as train


def main(argv):
    options = util.parseArguments(argv)
    train.startTrainingWithDataLoaders(options)


if __name__ == "__main__":
    util.writeLog("MAIN-PROCESS", "Scene Classification with Convolutional Neural Networks program is started.")
    start = time.time()
    main(sys.argv)
    util.writeLog("MAIN-PROCESS",
                  "Scene Classification with Convolutional Neural Networks program is finished in "
                  + str(time.time() - start) + " seconds.")
